#!/usr/bin/env python3

# ganec, a genetic algorithm yagi-uda antenna design program.
# Copyright (C) 2020  Fedja Beader

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import errno
import math
import os
import random
import time

from sortedcontainers import SortedList
from PyNEC import *
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Antenna settings (for simulation)
rod_conductivity = 3.7e7 # Aluminum (siemens/m = mhos/m)
speed_of_light = 3e8 # Speed of light in vacuum m/s
number_of_rods = 6 # forming the antenna
rod_radius = 2.5e-3 # 5 mm rod diameter
# rod_clearance should be at least 2*rod_radius+epsilon, NEC does not like overlapping wires.
rod_clearance = 0.01 + 2*rod_radius # Minimal distance between two rods
target_frequency = 300e6 # For fitness simulation [Hz]
target_wavelength = speed_of_light/target_frequency
characteristic_impedance = 50 # Of the conduit leading to the antenna
#impedance_matching_cutoff = 0.95 # Minimum allowed impedance mismatch

# Genetic algorithm settings
population_count = 20 # Number of individuals in population
tournament_selection_K = 3 # Number of draws from which to pick the best
number_of_iterations = 1+2000 # Number of GA iterations to perform
min_rod_length = 0.005 # The minimal length of rod. 0 seems to cause problems in sim.

# General settings
sessions_dir = "/tmp/ganec"
# How many iterations to skip before plotting the best design
iteration_plot_skip = max(100, math.ceil(number_of_iterations / 100))


# Miscallaneous helpers
def to_NEC_freq(freq):
    return freq / 1e6 # Everything in MHz.

# Program execution environment setup
# Make sure the global sessions directory exists, then pick the next available
# run ID and use that as the active session directory.
if not os.path.isdir(sessions_dir):
    os.makedirs(sessions_dir)

ganec_run_id = 0
session_dir = ""
while True:
    session_dir = "%s/%04d" % (sessions_dir, ganec_run_id)
    if not os.path.isdir(session_dir):
        break
    ganec_run_id += 1

os.makedirs(session_dir)
pictures_dir = session_dir + "/pictures"
os.makedirs(pictures_dir)




class Geometry:
    def __init__(self):
        self.wire_number = 0
        self.nec = nec_context()
        self.geo = self.nec.get_geometry()

    def add_row(self, line):
        #self.text.append(line)
        #print (line)
        pass

    def add_rod(self, offset, length, num_segments = 25):
        # Add a conductive rod to the common carrier
        #    <offset> is the offset from coordinate origin on the common
        #             carrier, which lies on the x axis
        #    <length> the total length of the rod
        # Line format: GW
        #   <wire number> - which wire this line belongs to
        #   <number of segments> - how many equidistant segments the line is split into (sim)
        #   <X> <Y> <Z> - coordinates of the first end of the wire (in meters)
        #   <X> <Y> <Z> - coordinates of the second end of the wire (in meters)
        #                 (Z points upwards, X,Y are horizontal)
        #   <radius> - the radius of this line

        self.wire_number += 1
        self.add_row("GW %5d %5d %5.5E %.5E %.5E %.5E %.5E %.5E %.5E" % (
          self.wire_number, num_segments,
          offset, length/2, 0,
          offset, -length/2, 0,
          rod_radius))

        self.geo.wire(self.wire_number, num_segments,
                      offset, length/2, 0,
                      offset, -length/2, 0,
                      rod_radius, 1.0, 1.0)


    def end_geometry(self, gpflag):
        # End the geometry entry and specify ground type
        #    <gpflag> - 0=no ground
        self.add_row("GE %5d %5d %5.5E %.5E %.5E %.5E %.5E %.5E %.5E" % (
            gpflag, 0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0))
        self.nec.geometry_complete(gpflag)

    def frequency(self, IFRQ, NFRQ, FMHz, DELFRQ):
        # Specify simulation frequencies
        #    <IFRQ> - frequency stepping mode (0-linear, 1-mult)
        #    <NFRQ> - num of freq. steps
        #    <FMHz> -
        #    <DELFRQ> - frequency increment(linear) or mult factor(mult)
        self.add_row("FR     %-5d %-5d %-5d %-5d %.5E %.5E %.5E %.5E %.5E %.5E" % (
          IFRQ, NFRQ, 0, 0,
          FMHz, DELFRQ, 0, 0, 0, 0))

        self.nec.fr_card(IFRQ, NFRQ, FMHz, DELFRQ)

    def excitation(self, etype, segtag, srcseg, volt):
        # Specify excitation of the structure
        #    <etype> 0=voltage source
        #    <
        self.add_row("EX     %-5d %-5d %-5d %-5d %.5E %.5E %.5E %.5E %.5E %.5E" % (
          etype, segtag, srcseg, 0,
          volt, 0, 0, 0, 0, 0))

        self.nec.ex_card(etype, segtag, srcseg, 0,
                         volt, 0.0, 0.0, 0.0, 0.0, 0.0)

    def loading(self, ldtyp, ldtag, ldtagf, ldtagt, ZLR, ZLI, ZLC):
        # Specify loading of the structure
        #    <etype> 0=voltage source
        #    <
        self.add_row("LD     %-5d %-5d %-5d %-5d %.5E %.5E %.5E %.5E %.5E %.5E" % (
          ldtyp, ldtag, ldtagf, ldtagt,
          ZLR, ZLI, ZLC, 0, 0, 0))

        self.nec.ld_card(ldtyp, ldtag, ldtagf, ldtagt, ZLR, ZLI, ZLC)

    def radiation_pattern(self, mode, NTH, NPH, XNDA, THETS, PHIS, DTH, DPH, RFLD, GNOR):
        # Request computation of the radiation pattern (RP)
        #    <mode> - 0=normal, antenna in free space unless a ground plane was added
        #    <NTH>  - number of values of theta at which to compute
        #    <NPH>  - ditto for phi
        #    <XNDA> - a base10 bit field
        #    <THETS>- initial theta angle in deg
        #    <PHIS> - initial phi angle in deg
        #    <DTH>  - increment for theta in degrees
        #    <DPH>  - increment for phi in degrees
        #    <RFLD> - radial distance
        #    <GNOR>
        self.add_row("RP %5d %5d %5d %6d %.5E %.5E %.5E %.5E %.5E %.5E" % (
          mode, NTH, NPH, XNDA,
          THETS, PHIS, DTH, DPH, RFLD, GNOR))
        self.nec.rp_card(calc_mode=mode, n_theta=NTH, n_phi=NPH,
          output_format=0, normalization=0, D=0, A=0,
          theta0= THETS, delta_theta=DTH,
          phi0= PHIS, delta_phi=DPH,
          radial_distance=RFLD, gain_norm=GNOR)

    def end(self):
        # End file
        self.add_row("EN     0     0     0      0  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00")

    def get_nec_context(self):
        return self.nec


def geometry():
    g = Geometry()

    g.add_rod(0.0, 2*0.484, 25)
    g.add_rod(-0.4,   2*0.509, 25)
    g.add_rod(0.3, 2*0.459, 22)
    g.add_rod(0.7, 2*0.45,  22)
    g.add_rod(1.1, 2*0.44,  22)
    g.add_rod(1.5, 2*0.43,  21)
    # Do not rotate, but translate geometry by x=-1
    #g.add_row("GM     0     0   0.00000E+00  0.00000E+00  0.00000E+00 -1.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00")
    g.end_geometry(0)
    g.frequency(0, 0, to_NEC_freq(target_frequency), 0.5)
    g.excitation(etype = 0, segtag = 1, srcseg = 13, volt = 1.0)
    g.loading(5, 0, 0, 0, rod_conductivity, 0.0, 0.0)

    # Near E/H field computation
    #g.add_row("NH     0     0     0      0  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00")
    #g.add_row("NE     0    20    15      1 -1.40000E+00 -1.40000E+00  5.00000E-02  2.00000E-01  2.00000E-01  0.00000E+00")

    #g.radiation_pattern(0, 37, 73, 1000,  0.0, 0.0, 5.0, 5., 0., 0.)
    g.end()

    return g


def testrun():
    g = geometry()
    nec = g.get_nec_context()

    nec.rp_card(calc_mode=0, radial_distance=0, gain_norm=0,
                output_format=0, normalization=0, D=0, A=0,
                theta0=90, delta_theta=5, n_theta=0,
                phi0=0, delta_phi=5, n_phi=0)
    #nec.rp_card(calc_mode=0, n_theta=37, n_phi=73, output_format=1, normalization=0, D=0, A=0, theta0=0, delta_theta=5, phi0=0, delta_phi=5, radial_distance=0, gain_norm=0)
    nec.xq_card(0) # Execute simulation

    #ipt = nec.get_input_parameters(0)
    #z = ipt.get_impedance()
    #print(("Impedance is {}".format(z)))

    rpt = nec.get_radiation_pattern(0)

    complex_e_field = rpt.get_e_theta()
    #e = complex_e_field.reshape((30,30))
    print((complex_e_field.size))

    print("max gain: " + str(nec.get_gain_max(0)))
    print("max gain: " + str(nec.get_gain(0, 18, 0)))
    print("max gain: " + str(nec.get_gain(0, 0, 0)))


#testrun()




class Individual:
    def __init__(self, iteration_of_birth, chromosome = -1):
        if chromosome == -1:
            # Initialize random individual
            chromosome = []
            for rod in range(0, number_of_rods):
                for gene in range(0, 2): # pos+len
                    chromosome.append(random.random())

        self.iteration_of_birth = iteration_of_birth
        self.chromosome = chromosome
        self.print()
        #self.__init__(chromosome) infinite loop?

    def __lt__(self, other):
        return self.get_fitness() < other.get_fitness()


    def age(self, current_iteration):
        return current_iteration - self.iteration_of_birth


    def print(self):
        line = "rods: "
        for rod in range(0, number_of_rods):
            line += ("[%d at %.3f, len %.3f], " %
              (rod, self.chromosome[2*rod], self.chromosome[2*rod+1]))
        print ("%sfitness: %.3f" % (line, self.get_fitness()))

    def plot(self, ax, iteration):
        # Plot rods
        for rod in range(0, number_of_rods):
            x = self.chromosome[2*rod]
            length = self.chromosome[2*rod + 1]

            if rod == 0:
                # Plot excitation element
                ax.plot(x, 0.5, 'o')

            ax.plot([x, x], [0.5 - length/2, 0.5 + length/2],
                    label = "off %f, len %f" % (x, length))
            ax.legend(loc = 'upper left')

        ax.set(xlabel='x', ylabel='y',
               title='Antenna, iter = %05d, fitness = %0.3f' % (iteration, self.get_fitness()))

    def compute_fitness(self):
        # Fix "GEOMETRY DATA ERROR -- WIRE #2 (TAG ID #2) INTERSECTS WIRE ..."
        # by extremely penalising such situations (-inf fitness).
        # n^2 dumb search for now, n is small enough.
        for rod1 in range(0, number_of_rods - 1):
            off1 = self.chromosome[2 * rod1]

            for rod2 in range(rod1+1, number_of_rods):
                off2 = self.chromosome[2 * rod2]
                # less than rod_clearance m distance between rods
                if abs (off1 - off2) < rod_clearance:
                    print ("rod %d too close to %d: %f" % (rod1, rod2, off1-off2))
                    self.fitness = -math.inf
                    return

            rod_len = self.chromosome[2 * rod1 + 1]
            if rod_len < min_rod_length:
                self.fitness = -math.inf
                return


        g = Geometry()

        excited_segment = -1
        for rod in range(0, number_of_rods):
            offset = self.chromosome[2 * rod]
            length = self.chromosome[2 * rod + 1]
            # From http://www.nec2.org/part_3/cards/gw.html:
            # "As a rule of thumb, segment lengths should be less than 0.1
            # wave-length at the desired frequency. Somewhat longer segments
            # may be used on long wires with no abrupt changes, while shorter
            # segments, 0.05 wavelength or less, may be required in modeling
            # critical regions of an antenna."
            nsegs = math.ceil (length / (0.05 * target_wavelength))
            # Make nsegs odd, to ensure that the active element is excited
            # exactly in the middle.
            if nsegs % 2 == 0:
                nsegs += 1
            #print ("len: %.3f, using %d segments" % (length, nsegs))
            g.add_rod(offset, length, nsegs)

            if rod == 0:
                excited_segment = int((nsegs - 1) / 2 + 1)

        g.end_geometry(0)
        # Single frequency simulation
        g.frequency(IFRQ = 0, NFRQ = 0, FMHz = to_NEC_freq(target_frequency), DELFRQ = 0.5)
        # Excite the first rod in the middle
        #print ("Will excite segment %d" % excited_segment)
        g.excitation(etype = 0, segtag = 1, volt = 1.0, srcseg = excited_segment)
        # Load all segments with same real resistance
        g.loading(5, 0, 0, 0, rod_conductivity, 0.0, 0.0)

        g.end()

        nec = g.get_nec_context()

        nec.rp_card(calc_mode=0, radial_distance=0, gain_norm=0,
                    output_format=0, normalization=0, D=0, A=0,
                    theta0=90, delta_theta=5, n_theta=0,
                    phi0=0, delta_phi=5, n_phi=0)
        nec.xq_card(0) # Execute simulation

        # Radiation pattern for the first simulated frequency
        rpt = nec.get_radiation_pattern(0)

        # Favorise antennas with a great impedance match to the conduit
        z = nec.get_input_parameters(0).get_impedance()
        reflection = (z - characteristic_impedance)/(z + characteristic_impedance)
        matching = 1 - abs(reflection)**2 # Called "neprilagoditev bremena" in literature?

        self.fitness = nec.get_gain(0, 0, 0)
        print("gain is %.3f dB, impedance is abs(%.3f%+.3fj) = %.3f, ref = abs(%.3f%+.3fj) = %.3f, matching = %.3f"
          % (self.fitness, z.real, z.imag, abs(z), reflection.real, reflection.imag, abs(reflection), matching))

        if matching <= 0:
            logmatching = -math.inf
        else:
            logmatching = 10*math.log10(matching)
        self.fitness = logmatching + self.fitness

        #print("max gain: " + str(self.fitness))


    def get_fitness(self):
        if not hasattr(self, 'fitness'):
            self.compute_fitness()

        return self.fitness


    def mutate(self):
        # Set a random gene to a random value
        rod = random.randrange(0, len(self.chromosome))
        self.chromosome[rod] = random.random()
        self.compute_fitness() # Force fitness recomputation.


    def mutate_border(self):
        # Set a random gene to a border value
        gene = random.randrange(0, len(self.chromosome))
        self.chromosome[gene] = random.randrange(0, 2)
        self.print()
        self.compute_fitness() # Force fitness recomputation.




class Population:
    def __init__(self, number_of_individuals):
        # Random population initialization. Invalid individuals
        # are rejected based on trial and error
        self.individuals = SortedList([])
        self.iteration_count = 0

        for ind in range(0, number_of_individuals):
            while True:
                try:
                    i = Individual(self.iteration_count)
                    self.individuals.add(i)
                    break
                except:
                    pass


    def ga_iterate(self):
        self.iteration_count += 1
        parentA_ind, parentA_fitness = pop.select_tournament_max()
        parentB_ind, parentB_fitness = pop.select_tournament_max()
        # If same parent is not allowed twice:
        #while True:
        #    if parentA_ind != parentB_ind:
        #        break
        #    parentB_ind, parentB_fitness = pop.select_tournament_max()

        # Apply crossover with a high probability
        if random.random() > 0.2:
            A, B = self.gene_crossover(self.individuals[parentA_ind], self.individuals[parentB_ind])
            self.individuals.add(A)
            self.individuals.add(B)


        # Apply recombination
        child1 = self.gene_recombination(self.individuals[parentA_ind], self.individuals[parentB_ind])

        # (exploration)
        mutation_chance = 0.2
        if random.random() < mutation_chance:
            child1.mutate()
        self.individuals.add(child1)

        # More exploration: explore borders
        outsider = Individual(self.iteration_count)
        outsider.mutate_border()
        self.individuals.add(outsider)

        # (exploration) Replace the least fittest (could be the child above)
        # with a random individual.
        self.individuals.add(Individual(self.iteration_count))

        while len(self.individuals) > population_count:
            index, fitness = pop.select_tournament_min(valid = lambda item: item.age(self.iteration_count) > 10)
            del self.individuals[index]


    def gene_crossover(self, parentA, parentB):
        # Apply crossover at a random point in the chromosome
        cross_ind = random.randrange(1, len(parentA.chromosome))
        print ("Crossing over at index %d " % cross_ind)

        chromoA = []
        chromoB = []
        for i in range(0, len(parentA.chromosome)):
            if i >= cross_ind:
                chromoB.append(parentA.chromosome[i])
                chromoA.append(parentB.chromosome[i])
            else:
                chromoA.append(parentA.chromosome[i])
                chromoB.append(parentB.chromosome[i])

        A = Individual(self.iteration_count, chromoA)
        B = Individual(self.iteration_count, chromoB)
        return A,B


    def gene_recombination(self, parentA, parentB):
        # Apply a recombination between chromosomes, producing an offspring
        chromosome = []
        alpha = 0.5
        for i in range(0, len(parentA.chromosome)):
            chromosome.append(
              alpha * parentA.chromosome[i] + (1-alpha) * parentB.chromosome[i])

        return Individual(self.iteration_count, chromosome)


    def index_of_least_fit(self):
        # Retrieve the least fit individual index
        return 0


    def index_of_most_fit(self):
        # Retrieve the most fit individual index
        return len(self.individuals) - 1


    def print(self):
        print ("")
        print ("Population in iteration %d" % self.iteration_count)
        for individual in self.individuals:
            individual.print()
        print ("")


    def select_random(self):
        index = random.randrange(0, len(self.individuals))
        fitness = self.individuals[index].get_fitness()
        #print ("select_random ind %d: %.4f" %(index, fitness))
        return index, fitness


    def select_tournament(self, num_draws, valid, compare):
        best_index, best_fitness = self.select_random()

        for i in range(1, num_draws):
            for x in range(0,3): # Three attempts
                index, fitness = self.select_random()
                if valid(self.individuals[index]):
                    break

            if compare(fitness, best_fitness):
                best_index, best_fitness = index, fitness

        #print ("select_tournament ind %d: %.4f" %(best_index, best_fitness))
        return best_index, best_fitness

    def select_tournament_min (self, valid):
        return self.select_tournament(2, valid, float.__lt__)

    def select_tournament_max(self, valid = lambda item: True):
        return self.select_tournament(tournament_selection_K, valid, float.__gt__)



pop = Population(population_count)

time_start = time.time()
for i in range(0, number_of_iterations):
    pop.ga_iterate()

    if i % iteration_plot_skip == 0:
        # Data for plotting
        fig, ax = plt.subplots()
        pop.individuals[pop.index_of_most_fit()].plot(ax, i)
        plt.xlim(0.0, 1.0)
        plt.ylim(0.0, 1.0)
        ax.grid()

        fig.savefig("%s/best_in_iter_%08d.png" % (pictures_dir, i))
        plt.close()
        #plt.show()
        pop.print()

time_end = time.time()
time_elapsed = time_end - time_start


print ("Finished in %d seconds." % (math.ceil(time_elapsed)))

